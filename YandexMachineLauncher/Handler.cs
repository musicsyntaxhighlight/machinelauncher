using Yandex.Cloud;
using Yandex.Cloud.Compute.V1;
using Yandex.Cloud.Credentials;
using Yandex.Cloud.Functions;

namespace YandexMachineLauncher;

public class Handler : YcFunction<string, string>
{
    public string FunctionHandler(string request, Context context)
    {
        var sdk = new Sdk(new MetadataCredentialsProvider());
        var startRequest = sdk.Services.Compute.InstanceService.Start(new StartInstanceRequest()
        {
            InstanceId = Environment.GetEnvironmentVariable("VM_ID")
        });

        return startRequest.ToString();
    }
}